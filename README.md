# DeFiant SSW #



### What is DeFiant SSW? ###

**DeFiant SSW** is a decentralized financial ecosystem consisting of dApps built on the **Solano** blockchain with **Serum**’s liquidity infrastructure and **Wormhole**’s cross-chain bridge.  